pipelineJob('Pipeline02') {

    def repo = 'https://bitbucket.org/kcrane/jhipster-basic-micro-mongo.git'

    definition {
        cpsScm{
            scm {
                git {
                    remote { url('https://bitbucket.org/kcrane/jhipster-basic-micro-mongo.git') }
                    branches('master')
                    extensions { }  // required as otherwise it may try to tag the repo, which you may not want
                }
            }    
            scriptPath('Jenkinsfile')
        }
        parameters {
            passwordParameterDefinition {
                name('SECRET_ACCESS_KEY')
                defaultValue(null)
                description('')
            }
            gitParameterDefinition {
                name('branch_selector')
                type('PT_BRANCH_TAG')
                defaultValue('origin/master')
                description('')
                branch('')
                branchFilter('.*')
                tagFilter('*')
                sortMode('ASCENDING')
                selectedValue('DEFAULT')
                useRepository('')
                quickFilterEnabled(false)
            }
        }
    }
}