//TODO add comment explaining the reason behind using getSerializedList
//TODO remove unused imports

import groovy.json.JsonSlurper
import groovy.json.JsonOutput
import groovy.json.JsonBuilder
import groovy.util.*

def getSerializedList(map) {
    
    def serializedList = []

    for (def entry in map) {
        serializedList.add(new java.util.AbstractMap.SimpleImmutableEntry(entry.key, entry.value))
    }

    return serializedList

}

def getJson(data){

    def jsonSlurper = new JsonSlurper()
    def json = jsonSlurper.parseText(data)
    jsonSlurper = null

    return json

}

def getPipelineConfig(){

    def workspacePath = "${new File(__FILE__).parent}"
    def pipelineConfigPath = workspacePath + "/pipeline-config.json"
    
    def data = readFileFromWorkspace(pipelineConfigPath)
    def json = getJson(data)

    return json

}

def getPipelineConfig2(){

    def slurper = new ConfigSlurper()
    // slurper.classloader = this.class.classloader
    def workspacePath = "${new File(__FILE__).parent}"
    def pipelineConfigPath = workspacePath + "/services.groovy"
    def config = slurper.parse(readFileFromWorkspace(pipelineConfigPath))
    
    return config


}

def getPipelineConfig3(){

    def slurper = new ConfigSlurper()
    def workspacePath = "${new File(__FILE__).parent}"
    // def pipelineConfigPath = workspacePath + "/pipelineConfig.groovy"
    // def pipelineConfigPath = workspacePath + "/pipeline-config.groovy"
    def pipelineConfigPath = workspacePath + "/services2.groovy"
    def config = slurper.parse(readFileFromWorkspace(pipelineConfigPath))
    // println("**********")
    // println(config)
    // println("**********")   
    // println(config.services)
    // println("**********")
    return config.services

}

def getPipelineConfig4(){

    def slurper = new ConfigSlurper()
    def workspacePath = "${new File(__FILE__).parent}"
    // def pipelineConfigPath = workspacePath + "/pipelineConfig.groovy"
    // def pipelineConfigPath = workspacePath + "/pipeline-config.groovy"
    def pipelineConfigPath = workspacePath + "/pipeline-config.groovy"
    def config = slurper.parse(readFileFromWorkspace(pipelineConfigPath))
    // println("**********")
    // println(config)
    // println("**********")   
    // println(config.services)
    // println("**********")
    return config.pipelineConfig

}

def buildPipeline(pipelineConfig, service){

    def definition = pipelineConfig.definition
    def passwordParameterDefinition = pipelineConfig.parameters.passwordParameterDefinition
    def gitParameterDefinition = pipelineConfig.parameters.gitParameterDefinition

    //Service
    def jobName = service.jobName
    def repository = service.repository

    pipelineJob(jobName) {

        // definition {
        //     cpsScm{
        //         scm {
        //             git {
        //                 def pipelineConfig2 = getPipelineConfig()
        //                 println(pipelineConfig2)
        //                 remote { url(repository) }
        //                 branches(definition.scm.branch)
        //                 extensions { }  // required as otherwise it may try to tag the repo, which you may not want
        //             }
        //         }    
        //         scriptPath(definition.scm.scriptPath)
        //     }
        // }

        definition {
            cpsScm{
                scm {
                    git {
                        remote { url('https://bitbucket.org/kcrane/jhipster-basic-micro-mongo.git') }
                        branches('master')
                        extensions { }  // required as otherwise it may try to tag the repo, which you may not want
                    }
                }    
                scriptPath('Jenkinsfile')
            }
        }

        parameters {
            passwordParameterDefinition {
                name(passwordParameterDefinition.name)
                defaultValue(passwordParameterDefinition.defaultValue)
                description(passwordParameterDefinition.description)
            }
            gitParameterDefinition {
                name(gitParameterDefinition.name)
                type(gitParameterDefinition.type)
                defaultValue(gitParameterDefinition.defaultValue)
                description(gitParameterDefinition.description)
                branch(gitParameterDefinition.branch)
                branchFilter(gitParameterDefinition.branchFilter)
                tagFilter(gitParameterDefinition.tagFilter)
                sortMode(gitParameterDefinition.sortMode)
                selectedValue(gitParameterDefinition.selectedValue)
                useRepository(gitParameterDefinition.useRepository)
                quickFilterEnabled(gitParameterDefinition.quickFilterEnabled)
            }
        }
    }
}

def buildPipeline2(service){

    println(service)

    pipelineJob(service.serviceJobName) {

        definition {
            cpsScm{
                scm {
                    git {
                        remote { url(service.serviceRepository) }
                        branches(service.branch)
                        extensions { }  // required as otherwise it may try to tag the repo, which you may not want
                    }
                }    
                scriptPath(service.scriptPath)
            }
        }

        parameters {
            passwordParameterDefinition {
                name(service.passwordName)
                defaultValue(service.passwordDefaultValue)
                description(service.passwordDescription)
            }
            gitParameterDefinition {
                name(service.gitParameterName)
                type(service.gitParameterType)
                defaultValue(service.gitParameterDefaultValue)
                description(service.gitParameterDescription)
                branch(service.gitParameterBranch)
                branchFilter(service.gitParameterBranchFilter)
                tagFilter(service.gitParameterTagFilter)
                sortMode(service.gitParameterSortMode)
                selectedValue(service.gitParameterSelectedValue)
                useRepository(service.gitParameterUseRepository)
                quickFilterEnabled(service.gitParameterQuickFilterEnabled)
            }
        }
    }
}

def buildPipeline3(pipelineConfig, service){

    def definition = pipelineConfig.definition
    def passwordParameterDefinition = pipelineConfig.parameters.passwordParameterDefinition
    def gitParameterDefinition = pipelineConfig.parameters.gitParameterDefinition
    def jobName = service.jobName
    println(jobName)
    def repository = service.repository
    println(repository)

    pipelineJob(jobName) {

        // definition {
        //     cpsScm{
        //         scm {
        //             git {
        //                 remote { url(repository) }
        //                 branches(definition.scm.branch)
        //                 extensions { }  // required as otherwise it may try to tag the repo, which you may not want
        //             }
        //         }    
        //         scriptPath(definition.scm.scriptPath)
        //     }
        // }

        definition {
            cpsScm{
                scm {
                    git {
                        remote { url('https://bitbucket.org/kcrane/jhipster-basic-micro-mongo.git') }
                        branches('master')
                        extensions { }  // required as otherwise it may try to tag the repo, which you may not want
                    }
                }    
                scriptPath('Jenkinsfile')
            }
        }

        parameters {
            passwordParameterDefinition {
                name(passwordParameterDefinition.name)
                defaultValue(passwordParameterDefinition.defaultValue)
                description(passwordParameterDefinition.description)
            }
            gitParameterDefinition {
                name(gitParameterDefinition.name)
                type(gitParameterDefinition.type)
                defaultValue(gitParameterDefinition.defaultValue)
                description(gitParameterDefinition.description)
                branch(gitParameterDefinition.branch)
                branchFilter(gitParameterDefinition.branchFilter)
                tagFilter(gitParameterDefinition.tagFilter)
                sortMode(gitParameterDefinition.sortMode)
                selectedValue(gitParameterDefinition.selectedValue)
                useRepository(gitParameterDefinition.useRepository)
                quickFilterEnabled(gitParameterDefinition.quickFilterEnabled)
            }
        }
    }
}

def buildPipeline4(service){

    pipelineJob(service.jobName) {

        definition {
            cpsScm{
                scm {
                    git {
                        remote { url(service.repository) }
                        branches(service.definition.scm.branch)
                        extensions { }  // required as otherwise it may try to tag the repo, which you may not want
                    }
                }    
                scriptPath(service.definition.scm.scriptPath)
            }
        }
        parameters {
            passwordParameterDefinition {
                name(service.parameters.passwordParameterDefinition.name)
                defaultValue(service.parameters.passwordParameterDefinition.defaultValue)
                description(service.parameters.passwordParameterDefinition.description)
            }
            gitParameterDefinition {
                name(service.parameters.gitParameterDefinition.name)
                type(service.parameters.gitParameterDefinition.type)
                defaultValue(service.parameters.gitParameterDefinition.defaultValue)
                description(service.parameters.gitParameterDefinition.description)
                branch(service.parameters.gitParameterDefinition.branch)
                branchFilter(service.parameters.gitParameterDefinition.branchFilter)
                tagFilter(service.parameters.gitParameterDefinition.tagFilter)
                sortMode(service.parameters.gitParameterDefinition.sortMode)
                selectedValue(service.parameters.gitParameterDefinition.selectedValue)
                useRepository(service.parameters.gitParameterDefinition.useRepository)
                quickFilterEnabled(service.parameters.gitParameterDefinition.quickFilterEnabled)
            }
        }
    }
}

def buildPipeline5(service){

    pipelineJob(service.jobName) {

        def scm = service.scm
        def passwordParameters = service.parameters.passwordParameterDefinition
        def gitParameters = service.parameters.gitParameterDefinition

        definition {
            cpsScm{
                scm {
                    git {
                        remote { url(service.repository) }
                        branches(scm.branch)
                        extensions { }  // required as otherwise it may try to tag the repo, which you may not want
                    }
                }    
                scriptPath(scm.scriptPath)
            }
        }
        parameters {
            passwordParameterDefinition {
                name(passwordParameters.name)
                defaultValue(passwordParameters.defaultValue)
                description(passwordParameters.description)
            }
            gitParameterDefinition {
                name(gitParameters.name)
                type(gitParameters.type)
                defaultValue(gitParameters.defaultValue)
                description(gitParameters.description)
                branch(gitParameters.branch)
                branchFilter(gitParameters.branchFilter)
                tagFilter(gitParameters.tagFilter)
                sortMode(gitParameters.sortMode)
                selectedValue(gitParameters.selectedValue)
                useRepository(gitParameters.useRepository)
                quickFilterEnabled(gitParameters.quickFilterEnabled)
            }
        }
    }
}

def buildPipeline6(pipelineConfig, service){

    pipelineJob(service.jobName) {

        definition {
            cpsScm{
                scm {
                    git {
                        remote { url(service.repository) }
                        branches(pipelineConfig.definition.scm.branch)
                        extensions { }  // required as otherwise it may try to tag the repo, which you may not want
                    }
                }    
                scriptPath(pipelineConfig.definition.scm.scriptPath)
            }
        }
        parameters {
            passwordParameterDefinition {
                name(pipelineConfig.parameters.passwordParameterDefinition.name)
                defaultValue(pipelineConfig.parameters.passwordParameterDefinition.defaultValue)
                description(pipelineConfig.parameters.passwordParameterDefinition.description)
            }
            gitParameterDefinition {
                name(pipelineConfig.parameters.gitParameterDefinition.name)
                type(pipelineConfig.parameters.gitParameterDefinition.type)
                defaultValue(pipelineConfig.parameters.gitParameterDefinition.defaultValue)
                description(pipelineConfig.parameters.gitParameterDefinition.description)
                branch(pipelineConfig.parameters.gitParameterDefinition.branch)
                branchFilter(pipelineConfig.parameters.gitParameterDefinition.branchFilter)
                tagFilter(pipelineConfig.parameters.gitParameterDefinition.tagFilter)
                sortMode(pipelineConfig.parameters.gitParameterDefinition.sortMode)
                selectedValue(pipelineConfig.parameters.gitParameterDefinition.selectedValue)
                useRepository(pipelineConfig.parameters.gitParameterDefinition.useRepository)
                quickFilterEnabled(pipelineConfig.parameters.gitParameterDefinition.quickFilterEnabled)
            }
        }
    }
}

def buildPipeline7(pipelineConfig, service){

    pipelineJob(service.jobName) {

        definition {
            cpsScm{
                scm {
                    git {

                        remote { url(service.repository) }
                        branches(pipelineConfig.definition.scm.branch)
                        extensions { }  // required as otherwise it may try to tag the repo, which you may not want
                        
                    }
                }    
                scriptPath(pipelineConfig.definition.scm.scriptPath)
            }
        }
        parameters {
            passwordParameterDefinition {

                def passwordParameterDefinition = pipelineConfig.parameters.passwordParameterDefinition

                name(pipelineConfig.parameters.passwordParameterDefinition.name)
                defaultValue(passwordParameterDefinition.defaultValue)
                description(passwordParameterDefinition.description)

            }
            gitParameterDefinition {
                
                def gitParameterDefinition = pipelineConfig.parameters.gitParameterDefinition

                name(gitParameterDefinition.name)
                type(gitParameterDefinition.type)
                defaultValue(gitParameterDefinition.defaultValue)
                description(gitParameterDefinition.description)
                branch(gitParameterDefinition.branch)
                branchFilter(gitParameterDefinition.branchFilter)
                tagFilter(gitParameterDefinition.tagFilter)
                sortMode(gitParameterDefinition.sortMode)
                selectedValue(gitParameterDefinition.selectedValue)
                useRepository(gitParameterDefinition.useRepository)
                quickFilterEnabled(gitParameterDefinition.quickFilterEnabled)

            }
        }
    }
}

def buildPipelineJobs(){

    // def pipelineConfig = getPipelineConfig()
    // def services = pipelineConfig.services

    // def serializedPipelineConfig = getSerializedList(pipelineConfig)
    // def serializedServices = getSerializedList(services)

    // for(service in services){

    //     buildPipeline(serializedPipelineConfig, serializedService)

    // }

    // def pipelineConfig = getPipelineConfig()
    // def services = pipelineConfig.services

    // for(service in services){

    //     println("service" + service)
    //     // println("service keys" + service.getKey())
    //     // def serviceKey = service.getKey()


    //     buildPipeline(pipelineConfig, service)

    // }

    //working
    // def pipelineConfig = getPipelineConfig2()
    // pipelineConfig.services.each{
    //     name, data -> buildPipeline2(data)
    // }

    //working
    // println("=====================")
    // def services = getPipelineConfig3()
    // println(services)
    // println("=====================")
    // services.each{
    //     service, data -> buildPipeline4(data)
    // }

    println("=====================")
    def pipelineConfig = getPipelineConfig4()
    println(pipelineConfig)
    println("=====================")
    pipelineConfig.services.each{
        service, data -> buildPipeline7(pipelineConfig, data)
    }

}

buildPipelineJobs()