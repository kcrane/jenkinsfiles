import groovy.util.*

def getPipelineConfig(){

    def slurper = new ConfigSlurper()
    def workspacePath = "${new File(__FILE__).parent}"
    def pipelineConfigPath = workspacePath + "/pipeline-config.groovy"
    def config = slurper.parse(readFileFromWorkspace(pipelineConfigPath))

    return config.pipelineConfig

}

def buildTestPipeline(pipelineConfig, service){

    pipelineJob(service.testName) {

        definition {
            cpsScm{
                scm {
                    git {

                        remote { url(service.repository) }
                        branches(pipelineConfig.definition.scm.branch)
                        extensions { }  // required as otherwise it may try to tag the repo, which you may not want
                        
                    }
                }    
                scriptPath(pipelineConfig.definition.scm.scriptPath)
            }
        }

        logRotator {
            numToKeep(pipelineConfig.logRotator.numToKeep)
        }

        triggers {
            scm(pipelineConfig.triggers.scm)
        }
    }
}

def buildPipelineJobs(){

    def pipelineConfig = getPipelineConfig()

    pipelineConfig.services.each{ service, data -> 
        buildTestPipeline(pipelineConfig, data)
    }

}

buildPipelineJobs()