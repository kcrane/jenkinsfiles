services {
        serv1{
            scriptPath = "Jenkinsfile"
            branch = "master"
            passwordName = "SECRET_ACCESS_KEY"
            passwordDefaultValue = null
            passwordDescription = ""
            gitParameterName = "${branch_selector}"
            gitParameterType = "PT_BRANCH_TAG"
            gitParameterDefaultValue = "origin/master"
            gitParameterDescription = ""
            gitParameterBranch = ""
            gitParameterBranchFilter = ".*"
            gitParameterTagFilter = "*"
            gitParameterSortMode = "ASCENDING"
            gitParameterSelectedValue = "DEFAULT"
            gitParameterUseRepository = ""
            gitParameterQuickFilterEnabled = false
            serviceJobName = "jhipster-basic-micro-mongo"
            serviceRepository = "https://bitbucket.org/kcrane/jhipster-basic-micro-mongo.git"
        }
        serv2{
            scriptPath = "Jenkinsfile"
            branch = "master"
            passwordName = "SECRET_ACCESS_KEY"
            passwordDefaultValue = null
            passwordDescription = ""
            gitParameterName = "${branch_selector}"
            gitParameterType = "PT_BRANCH_TAG"
            gitParameterDefaultValue = "origin/master"
            gitParameterDescription = ""
            gitParameterBranch = ""
            gitParameterBranchFilter = ".*"
            gitParameterTagFilter = "*"
            gitParameterSortMode = "ASCENDING"
            gitParameterSelectedValue = "DEFAULT"
            gitParameterUseRepository = ""
            gitParameterQuickFilterEnabled = false
            serviceJobName = "jhipster-basic-micro-mongo2"
            serviceRepository = "https://bitbucket.org/kcrane/jhipster-basic-micro-mongo.git"
        }

}