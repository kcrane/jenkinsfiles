pipelineConfig {
  definition {
    scm {
      scriptPath = "Jenkinsfile"
      branch = "master"
    }
  }
  parameters {
    passwordParameterDefinition {
      name = "SECRET_ACCESS_KEY"
      defaultValue = null
      description = ""
    }
    gitParameterDefinition {
      name = "\${branch_selector}"
      type = "PT_BRANCH_TAG"
      defaultValue = "origin/master"
      description = ""
      branch = ""
      branchFilter = ".*"
      tagFilter = "*"
      sortMode = "ASCENDING"
      selectedValue = "DEFAULT"
      userRepository = ""
      quickFilterEnabled = false      
    }
  }
  services {
    micro1 {
      jobName = "jhipster-basic-micro-mongo"
      repository = "https://bitbucket.org/kcrane/jhipster-basic-micro-mongo.git"
    }
  }

}