ecsConfig {
	jobName = "UPDATE_ECS_SERVICE2"
	repository = "https://bitbucket.org/kcrane/jhipster-basic-micro-mongo.git"
	definition {
	    scm {
	      scriptPath = "vars/ecsService.groovy"
	      branch = "master"
	    }
  	}
  	parameters {
	    passwordParameterDefinition {
	      name = "SECRET_ACCESS_KEY"
	      defaultValue = null
	      description = ""  
	    }
	}
}