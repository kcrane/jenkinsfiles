//TODO add comment explaining the reason behind using getSerializedList
//TODO remove unused imports

import groovy.json.JsonSlurper
import groovy.json.JsonOutput
import groovy.json.JsonBuilder

def getSerializedList(map) {
    
    def serializedList = []

    for (def entry in map) {
        serializedList.add(new java.util.AbstractMap.SimpleImmutableEntry(entry.key, entry.value))
    }

    return serializedList

}

def getJson(data){

    def jsonSlurper = new JsonSlurper()
    def json = jsonSlurper.parseText(data)

    return json

}

def getPipelineConfig(){

    def workspacePath = "${new File(__FILE__).parent}"
    def pipelineConfigPath = workspacePath + "/pipeline-config.json"
    
    def data = readFileFromWorkspace(pipelineConfigPath)
    def json = getJson(data)

    return json

}

def getPipelineConfig2(){


    def jsonBuilder = new JsonBuilder([
        scriptPath:"Jenkinsfile",
        branch:"master",
        passwordName:"SECRET_ACCESS_KEY",
        passwordDefaultValue:null,
        passwordDescription:"",
        gitParameterName:"${branch_selector}",
        gitParameterType:"PT_BRANCH_TAG",
        gitParameterDefaultValue:"origin/master",
        gitParameterDescription:"",
        gitParameterBranch:"",
        gitParameterBranchFilter:".*",
        gitParameterTagFilter:"*",
        gitParameterSortMode:"ASCENDING",
        gitParameterSelectedValue:"DEFAULT",
        gitParameterUseRepository:"",
        gitParameterQuickFilterEnabled:false,
        serviceJobName:"jhipster-basic-micro-mongo",
        serviceRepository:"https://bitbucket.org/kcrane/jhipster-basic-micro-mongo.git"
    ])
    
    return jsonBuilder


}

def buildPipeline(pipelineConfig, service){

    def definition = pipelineConfig.definition
    def passwordParameterDefinition = pipelineConfig.parameters.passwordParameterDefinition
    def gitParameterDefinition = pipelineConfig.parameters.gitParameterDefinition

    //Service
    def jobName = service.jobName
    def repository = service.repository

    pipelineJob(jobName) {

        // definition {
        //     cpsScm{
        //         scm {
        //             git {
        //                 def pipelineConfig2 = getPipelineConfig()
        //                 println(pipelineConfig2)
        //                 remote { url(repository) }
        //                 branches(definition.scm.branch)
        //                 extensions { }  // required as otherwise it may try to tag the repo, which you may not want
        //             }
        //         }    
        //         scriptPath(definition.scm.scriptPath)
        //     }
        // }

        definition {
            cpsScm{
                scm {
                    git {
                        remote { url('https://bitbucket.org/kcrane/jhipster-basic-micro-mongo.git') }
                        branches('master')
                        extensions { }  // required as otherwise it may try to tag the repo, which you may not want
                    }
                }    
                scriptPath('Jenkinsfile')
            }
        }

        parameters {
            passwordParameterDefinition {
                name(passwordParameterDefinition.name)
                defaultValue(passwordParameterDefinition.defaultValue)
                description(passwordParameterDefinition.description)
            }
            gitParameterDefinition {
                name(gitParameterDefinition.name)
                type(gitParameterDefinition.type)
                defaultValue(gitParameterDefinition.defaultValue)
                description(gitParameterDefinition.description)
                branch(gitParameterDefinition.branch)
                branchFilter(gitParameterDefinition.branchFilter)
                tagFilter(gitParameterDefinition.tagFilter)
                sortMode(gitParameterDefinition.sortMode)
                selectedValue(gitParameterDefinition.selectedValue)
                useRepository(gitParameterDefinition.useRepository)
                quickFilterEnabled(gitParameterDefinition.quickFilterEnabled)
            }
        }
    }
}

def buildPipelineJobs(){

    // def pipelineConfig = getPipelineConfig()
    // def services = pipelineConfig.services

    // def serializedPipelineConfig = getSerializedList(pipelineConfig)
    // def serializedServices = getSerializedList(services)

    // for(service in services){

    //     buildPipeline(serializedPipelineConfig, serializedService)

    // }

    // def pipelineConfig = getPipelineConfig()
    // def services = pipelineConfig.services

    // for(service in services){

    //     println("service" + service)
    //     // println("service keys" + service.getKey())
    //     // def serviceKey = service.getKey()


    //     buildPipeline(pipelineConfig, service)

    // }

    def pipelineConfig = getPipelineConfig()
    def jsonBuilder = new JsonBuilder(pipelineConfig)
    println(jsonBuilder)
    def services = pipelineConfig.services

    for(service in services){

        println("service" + service)
        // println("service keys" + service.getKey())
        // def serviceKey = service.getKey()


        buildPipeline(pipelineConfig, service)

    }

}

buildPipelineJobs()