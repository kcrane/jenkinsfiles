
import groovy.json.JsonSlurper;

def getJson(data){

    def jsonSlurper = new JsonSlurper()
    def json = jsonSlurper.parseText(data)

    println(json)

    return json

}

def getPipelineConfig(){

    def workspacePath = "${new File(__FILE__).parent}"
    def pipelineConfigPath = workspacePath + "/pipeline-config.json"
    
    def data = readFileFromWorkspace(pipelineConfigPath)
    def json = getJson(data)

    return json

}

def buildPipeline(pipelineConfig, service){

    def definition = pipelineConfig.definition
    def passwordParameterDefinition = pipelineConfig.parameters.passwordParameterDefinition
    def gitParameterDefinition = pipelineConfig.parameters.gitParameterDefinition
    def jobName = service.jobName

    println(jobName)

    pipelineJob(jobName) {

        definition {
            cpsScm{
                scm {
                    git {
                        println(service.repository)
                        remote { url(service.repository) }
                        branches(definition.scm.branch)
                        extensions { }  // required as otherwise it may try to tag the repo, which you may not want
                    }
                }    
                scriptPath(definition.scm.scriptPath)
            }
            
            parameters {
                passwordParameterDefinition {
                    name(passwordParameterDefinition.name)
                    defaultValue(passwordParameterDefinition.defaultValue)
                    description(passwordParameterDefinition.description)
                }
                gitParameterDefinition {
                    name(gitParameterDefinition.name)
                    type(gitParameterDefinition.type)
                    defaultValue(gitParameterDefinition.defaultValue)
                    description(gitParameterDefinition.description)
                    branch(gitParameterDefinition.branch)
                    branchFilter(gitParameterDefinition.branchFilter)
                    tagFilter(gitParameterDefinition.tagFilter)
                    sortMode(gitParameterDefinition.sortMode)
                    selectedValue(gitParameterDefinition.selectedValue)
                    useRepository(gitParameterDefinition.useRepository)
                    quickFilterEnabled(gitParameterDefinition.quickFilterEnabled)
                }
            }
        }
    }
}


def buildPipelineJobs(){

    def pipelineConfig = getPipelineConfig()
    def services = pipelineConfig.services

    for(Object service: services){

        buildPipeline(pipelineConfig, service)        

    }

}

buildPipelineJobs()
