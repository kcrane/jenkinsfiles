//TODO remove unused imports

import groovy.json.JsonSlurper
import groovy.json.JsonOutput
import groovy.json.JsonBuilder
import groovy.util.*

def getJson(data){

    def jsonSlurper = new JsonSlurper()
    def json = jsonSlurper.parseText(data)

    return json

}

def getPipelineConfig(){

    def workspacePath = "${new File(__FILE__).parent}"
    def pipelineConfigPath = workspacePath + "/pipeline-config.json"
    
    def data = readFileFromWorkspace(pipelineConfigPath)
    def json = getJson(data)

    return json

}

def getPipelineConfig4(){

    def slurper = new ConfigSlurper()
    def workspacePath = "${new File(__FILE__).parent}"
    def pipelineConfigPath = workspacePath + "/pipeline-config.groovy"
    def config = slurper.parse(readFileFromWorkspace(pipelineConfigPath))

    return config

}

def buildPipeline(pipelineConfig, service){

    def jobName = service.jobName

    pipelineJob(jobName) {

        def definition = pipelineConfig.definition
        def passwordParameterDefinition = pipelineConfig.parameters.passwordParameterDefinition
        def gitParameterDefinition = pipelineConfig.parameters.gitParameterDefinition

        definition {
            cpsScm{
                scm {
                    git {
                        remote { url(service.repository) }
                        branches(pipelineConfig.definition.scm.branch)
                        extensions { }  // required as otherwise it may try to tag the repo, which you may not want
                    }
                }    
                scriptPath(pipelineConfig.definition.scm.scriptPath)
            }
        }

        parameters {
            passwordParameterDefinition {
                name(passwordParameterDefinition.name)
                defaultValue(passwordParameterDefinition.defaultValue)
                description(passwordParameterDefinition.description)
            }
            gitParameterDefinition {
                name(gitParameterDefinition.name)
                type(gitParameterDefinition.type)
                defaultValue(gitParameterDefinition.defaultValue)
                description(gitParameterDefinition.description)
                branch(gitParameterDefinition.branch)
                branchFilter(gitParameterDefinition.branchFilter)
                tagFilter(gitParameterDefinition.tagFilter)
                sortMode(gitParameterDefinition.sortMode)
                selectedValue(gitParameterDefinition.selectedValue)
                useRepository(gitParameterDefinition.useRepository)
                quickFilterEnabled(gitParameterDefinition.quickFilterEnabled)
            }
        }
    }
}

def buildPipelineJobs(){

    println("=====================")
    def pipelineConfig = getPipelineConfig4()
    println(pipelineConfig.pipelineConfig.services)
    println("=====================")
    pipelineConfig.pipelineConfig.services.each{
        name, service -> buildPipeline(pipelineConfig.pipelineConfig, service)
    }
    // pipelineConfig.services.each{
    //     name, service -> buildPipeline(pipelineConfig, service)
    // }
    // for(service in pipelineConfig.services){
    //     buildPipeline(pipelineConfig, service)
    // }
}

buildPipelineJobs()